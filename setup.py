from setuptools import setup


setup(
    name='FF Prompt',
    version='0.1',
    scripts=['ff.py'],
    install_requires=['prompt-toolkit==1.0.7'],
    entry_points={
        'console_scripts': ['ff=ff:main']
    }
)
