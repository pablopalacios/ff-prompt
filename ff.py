from prompt_toolkit import prompt
from prompt_toolkit.contrib.completers import WordCompleter
from prompt_toolkit.contrib.regular_languages.compiler import compile
from prompt_toolkit.contrib.regular_languages.completion import GrammarCompleter
from prompt_toolkit.contrib.regular_languages.lexer import GrammarLexer
from prompt_toolkit.layout.lexers import SimpleLexer
from prompt_toolkit.styles import style_from_dict
from prompt_toolkit.token import Token

import math


operators = ['piolho', 'pablo', 'jojo']


def create_grammar():
    return compile("""
        (\s* (?P<operator>[a-z]+) \s+ \+ \s+ (?P<operator>[a-z]+) \s+ \+ \s+ (?P<operator>[a-z]+) \s*) |
        (\s* (?P<operator>[a-z]+) \s+ \+ \s+ (?P<operator>[a-z]+) \s*) |
        (\s* (?P<operator>[a-z]+) \s*)
    """)


def main():
    g = create_grammar()
    lexer = GrammarLexer(g, lexers={
        'operator': SimpleLexer(Token.Operator),
    })
    completer = GrammarCompleter(g, {
        'operator': WordCompleter(operators),
    })
    style = style_from_dict({
        Token.Operator: '#33aa33 bold',
        Token.Number: '#aa3333 bold',
        Token.TrailingInput: 'bg:#662222 #ffffff',
    })

    try:
        # REPL loop.
        while True:
            # Read input and parse the result.
            text = prompt(
                'ff> ', lexer=lexer, completer=completer, style=style)
            m = g.match(text)
            if m:
                vars = m.variables().getall('operator')
            else:
                print('Invalid command\n')
                continue
            if vars:
                vars.sort()
                result = {
                    ('jojo',): 'linda',
                    ('piolho',): 'janelinha',
                    ('pablo',): 'chato',
                    ('jojo', 'pablo'): 's2',
                    ('jojo', 'piolho'): 'lambibeijos',
                    ('pablo', 'piolho'): 'Grrrr',
                    ('jojo', 'pablo', 'piolho'): 'família feliz',
                }.get(tuple(vars))
                if result is None:
                    print('Comando inválido')
                    continue
                print(result)

    except EOFError:
        pass
